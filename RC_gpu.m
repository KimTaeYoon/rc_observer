clear all; close all; clc;
%%xc,yc,zc ==>0.01 300,30,400,1.3, 0.2, 0.1, 2, 3e-06, 0.6, 1
% function [x,y,z,g,pred_S] = RC_observer(A,B,C,time_step,T_training,T_prediction, node, spectral, degree, learning_rate, input_range, beta, bias, seed)
time_step=0.01; T_training=200; T_prediction=10; node=5000; spectral=1.3; degree=0.2; learning_rate=0.1; input_range=2; beta=3e-06; bias=0.6; seed=1; 
% x1=load(); y1=load(B); z1=load(C);
x1=load('xc'); y1=load('yc'); z1=load('zc');
x2=struct2cell(x1); y2=struct2cell(y1); z2=struct2cell(z1);
x=cell2mat(x2); y=cell2mat(y2); z=cell2mat(z2);

% g=0:time_step:(T_prediction/time_step)*time_step-time_step;
x=gpuArray(x);
% matrix A
matrix_A=ER_graph(node,degree,seed); % make Erdos-Reyni graph
A=(spectral/abs(max(eig(matrix_A))))*matrix_A; % make rescaled ER matrix
A=gpuArray(A);
% node's state
r=zeros(node,1);
r=gpuArray(r);
%initial input weights
W_in=random('Uniform',-input_range,input_range,node,1);
W_in=gpuArray(W_in);
%bias
B=ones(node,1);
B=gpuArray(B);
R=zeros(node,(T_training/time_step)+(T_prediction/time_step));
R=gpuArray(R);
tic
% each time step's state
for i = 1:(T_training/time_step)+(T_prediction/time_step)
    r=(1-learning_rate)*r+learning_rate*tanh(A*r+W_in*x(i)+bias*B);
    R(:,i)=r;
end

% target output
S=[y(1:(T_training/time_step)); z(1:(T_training/time_step))];

% matrix R&S
matrix_R=R(:,1:(T_training/time_step))-((1/(T_training/time_step))*sum(R(:,1:(T_training/time_step)),2));
matrix_S=S-((1/(T_training/time_step))*sum(S,2));

% weighted output matrix
W_out=(matrix_S*matrix_R')/(matrix_R*matrix_R' + beta*eye(node,node));

%constant C
const_C = -((W_out*((1/(T_training/time_step))*sum(R(:,1:(T_training/time_step)),2))-((1/(T_training/time_step))*sum(S,2))));
size(W_out)
%extract prdicting value by using reservoir system
pred_S=W_out*R(:,1+(T_training/time_step):(T_training/time_step)+(T_prediction/time_step))+const_C;
toc
% plot(g,y(T_training/time_step+1:(T_training/time_step)+(T_prediction/time_step))); hold on;
% plot(g,pred_S(1,:),'r'); figure;
% plot(g,z(T_training/time_step+1:(T_training/time_step)+(T_prediction/time_step))); hold on;
% plot(g,pred_S(2,:),'r'); %figure;

% end
