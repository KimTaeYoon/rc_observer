%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%Normal ER%%
%N=node, p=probability, seed = random seed number
function G = ER_graph(N,D,seed)
    %reservoir Erdos-Reyni matrix
    % N=10;D=5;
    rng(seed);
    A=zeros(N,N);
    p=0.3;
    while 1
        for j = 1:N
            for i = 1:j
                if A(i,j)==0 && rand <= p%(p^D)*((1-p)^(nchoosek(N,2)-D))
                    A(i,j)=rand;
                end
                if nnz(A)==D
                    break
                end
            end
            if nnz(A)==D
                break
            end
        end
        G=A+A'-eye(N,N).*A;
        if nnz(A)==D
            break
        end
    end
end