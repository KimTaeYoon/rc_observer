%%xc,yc,zc ==>0.01 300,30,400,1.3, 0.2, 0.1, 2, 3e-06, 0.6, 1
function [x,y,z,g,pred_S] = RC_observer_AIstudy(A,B,C,D,time_step,T_training,T_prediction, node, spectral, degree, learning_rate, input_range, beta, bias, seed)
data=load('/)
x=A';y=B';z=C';f=D';
% x=load(A); y=load(B); z=load(C);
% x2=struct2cell(x1); y2=struct2cell(y1); z2=struct2cell(z1);
% x=cell2mat(x2); y=cell2mat(y2); z=cell2mat(z2);

g=0:time_step:(T_prediction/time_step)*time_step-time_step;

% matrix A
matrix_A=ER_graph(node,degree,seed); % make Erdos-Reyni graph
A=(spectral/abs(max(eig(matrix_A))))*matrix_A; % make rescaled ER matrix

% node's state
r=zeros(node,1);

%initial input weights
W_in=random('Uniform',-input_range,input_range,node,3);

%bias
B=ones(node,1);

R=zeros(node,(T_training/time_step)+(T_prediction/time_step));

% each time step's state
for i = 1:(T_training/time_step)+(T_prediction/time_step)
    r=(1-learning_rate)*r+learning_rate*tanh(A*r+W_in*[x(i);y(i);z(i)] +bias*B);
    R(:,i)=r;
end

% target output
S=f(1:(T_training/time_step));
% matrix R&S
matrix_R=R(:,1:(T_training/time_step))-((1/(T_training/time_step))*sum(R(:,1:(T_training/time_step)),2));
matrix_S=S-((1/(T_training/time_step))*sum(S,2));
% weighted output matrix
W_out=(matrix_S*matrix_R')/(matrix_R*matrix_R' + beta*eye(node,node));

%constant C
const_C = -((W_out*((1/(T_training/time_step))*sum(R(:,1:(T_training/time_step)),2))-((1/(T_training/time_step))*sum(S,2))));
size(W_out)
%extract prdicting value by using reservoir system
pred_S=W_out*R(:,1+(T_training/time_step):(T_training/time_step)+(T_prediction/time_step))+const_C;

plot(g,f(T_training/time_step+1:(T_training/time_step)+(T_prediction/time_step))); hold on;
plot(g,pred_S(1,:),'r');
% plot(g,z(T_training/time_step+1:(T_training/time_step)+(T_prediction/time_step))); hold on;
% plot(g,pred_S(2,:),'r'); %figure;

end
