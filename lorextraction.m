
close all; clear all; clc;

X0=[2, 3, 5]; %initial parameter
t0=0; %initial time
h=0.01; %time step
T1=500/h; %total cal time
T3=10000/h;
v=[];

%loop rk4
for i = 1 : T1
    X1=rk4(t0,h,X0); t1=t0+h;
    v=[v; h*i, X1];
    X0=X1;t0=t1;
end

imx = mean(v(:,2));
istdx = std(v(:,2));
nx = (v(:,2)-imx)/istdx;

imy = mean(v(:,3));
istdy = std(v(:,3));
ny = (v(:,3)-imy)/istdx;

imz = mean(v(:,4));
istdz = std(v(:,4));
nz = (v(:,4)-imz)/istdz;

xx=imx; ssdx=istdx;
yy=imy; ssdy=istdy;
zz=imz; ssdz=istdz;

%normalize 
for k = T1 + 1 : 1 :  T1 + T3
    
    X1=rk4(t0,h,X0); t1=t0+h;
    v=[h*i, X1];
    X0=X1;t0=t1;
    
    xx=((k-1)*xx+v(:,2))/k;
    ssdx=sqrt(((k-1)*(ssdx^2)+((v(:,2)-xx)^2))/k);
    n_x(k)=(v(:,2)-xx)/ssdx;
    
    yy=((k-1)*yy+v(:,3))/k;
    ssdy=sqrt(((k-1)*(ssdy^2)+((v(:,3)-yy)^2))/k);
    n_y(k)=(v(:,3)-yy)/ssdy;
    
    zz=((k-1)*zz+v(:,4))/k;
    ssdz=sqrt(((k-1)*(ssdz^2)+((v(:,4)-zz)^2))/k);
    n_z(k)=(v(:,4)-zz)/ssdz;
    
end



function func=ft(t,p);
ap=10; be=28; rho=8/3;
func=[ap*(p(2)-p(1)),p(1)*(be-p(3))-p(2),p(1)*p(2)-rho*p(3)];
end

function Y=rk4(t,h,X);
s1=ft(t,X);
s2=ft(t+h/2,X+(h/2)*s1);
s3=ft(t+h/2,X+(h/2)*s2);
s4=ft(t+h,X+h*s3);
Y=X+(h/6)*(s1+2*s2+2*s3+s4);
end