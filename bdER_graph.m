%Bidirection ER%%
% N=node, p=probability, seed = random seed number
function G = bdER_graph(N,D,seed)
%reservoir Erdos-Reyni matrix
% N=10;D=5;
rng(seed);
G=zeros(N,N);
p=0.3;
while 1
    for j = 1:N
        for i = 1:N
            if G(i,j)==0 && rand <= p%(p^D)*((1-p)^(nchoosek(N,2)-D))
                G(i,j)=rand;
            end
            if nnz(G)==D
                break
            end
        end
        if nnz(G)==D
            break
        end
    end
    if nnz(G)==D
        break
    end
end
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%bidirection ER%%

% function G = ER_graph(N,D,seed)
% %reservoir Erdos-Reyni matrix
% rng(seed);
% p=D/N;
% if N > 5
%     G = sprand(N,N,p,p)+0;
% else
%     A = sprand(N,N,p,p)+0;
%     B = spones(A)+0;
%     G = A.*2-B;
% end
% end