close all; clear all; clc;

X0=[2, 3, 5]; %initial parameter
t0=0; %initial time
h=0.01; %time step
T1=800/h; %total cal time
v=[];

%loop rk4
for i = 1 : T1;
    X1=rk4(t0,h,X0); t1=t0+h;
    v=[v; h*i, X1];
    X0=X1;t0=t1;
end

f = figure;
ax = axes;
hold on

lo=animatedline;
lo.LineStyle='-';
lo.Color=[0.6 0.6 0.6];
lo.LineWidth=0.3;
lopoint=gca;
sz=30;
clr=[0.8 0.8 0.2];
ls=scatter(lopoint,nan,nan,sz,clr);
x = v(:,2);
y = v(:,3);
for k = 62300:length(x)
    addpoints(lo,x(k),y(k)); 
    set(ls,'xdata',x(k),'ydata',y(k))
    drawnow
end

function func=ft(t,p);
ap=10; be=23; rho=8/3;
func=[ap*(p(2)-p(1)),p(1)*(be-p(3))-p(2),p(1)*p(2)-rho*p(3)];
end

function Y=rk4(t,h,X);
s1=ft(t,X);
s2=ft(t+h/2,X+(h/2)*s1);
s3=ft(t+h/2,X+(h/2)*s2);
s4=ft(t+h,X+h*s3);
Y=X+(h/6)*(s1+2*s2+2*s3+s4);
end