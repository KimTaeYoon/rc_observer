%%xc,yc,zc ==>0.01 300,30,400,1.3, 0.2, 0.1, 2, 3e-06, 0.6, 1
function [RMS_error_y,RMS_error_z] = RC_observer(A,B,C,time_step,T_training,T_prediction, node, spectral, degree, learning_rate, input_range, beta, bias, seed)

    x1=load(A); y1=load(B); z1=load(C);
    x2=struct2cell(x1); y2=struct2cell(y1); z2=struct2cell(z1);
    x=cell2mat(x2); y=cell2mat(y2); z=cell2mat(z2);
    
    g=0:time_step:(T_prediction/time_step)*time_step-time_step;
    
    % matrix A
    matrix_A=bdER_graph(node,degree,seed); % make Erdos-Reyni graph
    A=(spectral/abs(max(eig(matrix_A))))*matrix_A; % make rescaled ER matrix
    
    % node's state
    r=zeros(node,1);
    
    %initial input weights
    W_in=random('Uniform',-input_range,input_range,node,1);
    W_ru=random('Uniform',-input_range,input_range,node,1);
    W_rx=random('Uniform',-input_range,input_range,node,1);
    W_ku=random('Uniform',-input_range,input_range,node,1);
    W_kx=random('Uniform',-input_range,input_range,node,1);
    W_xu=random('Uniform',-input_range,input_range,node,1);
    W_xx=random('Uniform',-input_range,input_range,node,1);

    %bias
    B=ones(node,1);
    
    R=zeros(node,(T_training/time_step)+(T_prediction/time_step));
    
    % each time step's state
    for i = 1:(T_training/time_step)+(T_prediction/time_step)
        k=sigmoid(W_ru*x(i)+W_rx*r+bias*B);
        kk=sigmoid(W_ku*x(i)+W_kx*r+bias+B);
        tr=tanh(W_xu*x(i)+W_xx*(k.*r)+bias*B);
        r=(1-kk).*r+kk.*tr;
        R(:,i)=r;
    end
    
    % target output
    S=[y(1:(T_training/time_step)); z(1:(T_training/time_step))];
    
    % matrix R&S
    matrix_R=R(:,1:(T_training/time_step))-((1/(T_training/time_step))*sum(R(:,1:(T_training/time_step)),2));
    matrix_S=S-((1/(T_training/time_step))*sum(S,2));
    
    % weighted output matrix
    W_out=(matrix_S*matrix_R')/(matrix_R*matrix_R' + beta*eye(node,node));
    
    %constant C
    const_C = -((W_out*((1/(T_training/time_step))*sum(R(:,1:(T_training/time_step)),2))-((1/(T_training/time_step))*sum(S,2))));
    
    %extract prdicting value by using reservoir system
    pred_S=W_out*R(:,1+(T_training/time_step):(T_training/time_step)+(T_prediction/time_step))+const_C;
    
    plot(g,y(T_training/time_step+1:(T_training/time_step)+(T_prediction/time_step))); hold on;
    plot(g,pred_S(1,:),'r'); figure;
    plot(g,z(T_training/time_step+1:(T_training/time_step)+(T_prediction/time_step))); hold on;
    plot(g,pred_S(2,:),'r'); %figure;
    
    RMS_error_y=sqrt((sum((y(T_training/time_step+1:(T_training/time_step)+(T_prediction/time_step))-pred_S(1,:)).^2))/length(pred_S(1,:)));
    RMS_error_z=sqrt((sum((z(T_training/time_step+1:(T_training/time_step)+(T_prediction/time_step))-pred_S(2,:)).^2))/length(pred_S(2,:)));
    end
    