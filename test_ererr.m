clear all; close all; clc;

bd_RMS_error_y=zeros(1,401);
bd_RMS_error_z=zeros(1,401);
A='/Users/Kcore/YOONGIT_RC/xc.mat';
B='/Users/Kcore/YOONGIT_RC/yc.mat';
C='/Users/Kcore/YOONGIT_RC/zc.mat';
Y=[];Z=[];
for ii = 1:10
    parfor i =1 : 401
        D=200*i;
        [ry,rz]=RC_observer(A,B,C,0.01,300,30,400,1.3,D,0.1,2,3e-06,0.6,1);
        bd_RMS_error_y(i)=ry;
        bd_RMS_error_z(i)=rz;
    end
    Y=[Y;bd_RMS_error_y];
    Z=[Z;bd_RMS_error_z];
end
Y=mean(Y,1);
Z=mean(Z,1);
plot(Y); figure;
plot(Z);
save('bd_RMS_error_y_mean','Y');
save('bd_RMS_error_z_mean','Z');